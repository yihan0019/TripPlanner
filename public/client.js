document.addEventListener("DOMContentLoaded", function() {
  var mouse = { 
     click: false,
     move: false,
     pos: {x:0, y:0},
     pos_prev: false
  };

  var currentDayNumber = 1;
  

  // get canvas element and create context
  var scheduleCanvas = document.getElementById('scheduleCanvas');
  var addDayButton = document.getElementById('addDay');
  var scheduleTable = document.getElementById('scheduleTable');
  var drawingCanvas  = document.getElementById('drawing');
  var drawingCanvasContext = drawingCanvas.getContext('2d');
  var messageField  = document.getElementById('messages');
  //var messageBox = document.getElementById("messageBox");
  var messageButton = document.getElementById('submitMessage');
  var messageInput = document.getElementById('messageInput');
  var width   = window.innerWidth;
  var height  = window.innerHeight;
  var socket  = io.connect();

  // set canvas to full browser width/height
  drawingCanvas.width = 300;
  drawingCanvas.height = 300;

  /*
  // register mouse event handlers
  drawingCanvas.onmousedown = function(e){ mouse.click = true; };
  drawingCanvas.onmouseup = function(e){ mouse.click = false; };

  drawingCanvas.onmousemove = function(e) {
     // normalize mouse position to range 0.0 - 1.0
     mouse.pos.x = e.clientX / width;
     mouse.pos.y = e.clientY / height;
     mouse.move = true;
  };
  */


/*
  addDayButton.onclick = function (e) {
    socket.emit('add_row');
    return true;
    
  }

  socket.on('add_row_server_side', function(){
    
    console.log("add row");
    var row = scheduleTable.insertRow(-1);
    var lastRowIndex = scheduleTable.rows.length-1;
    var lastRow =  scheduleTable.rows[lastRowIndex];

    for(i=0; i<5; i++){
      var newCell = row.insertCell(i);
      if(i==0){
        newCell.innerHTML = "Day " + lastRowIndex;
      } else {
        var inputField = document.createElement("input");
        inputField.className = "scheduleInput";
        newCell.appendChild(inputField);
      }
    }

    if(lastRowIndex == 2){
      var deleteDayButton = document.createElement("button");
      deleteDayButton.className = "toolButton";
      deleteDayButton.id = "deleteDayButton";
      scheduleCanvas.appendChild(deleteDayButton);
      deleteDayButton.innerHTML = "Delete";
      deleteDayButton.style.position = "absolute";
      deleteDayButton.style.top = addDayButton.style.top + lastRowIndex*25+'px';
      deleteDayButton.style.left = addDayButton.style.left; 
    } 

    deleteDayButton = document.getElementById('deleteDayButton');
    deleteDayButton.style.position = "absolute";
    deleteDayButton.style.top = addDayButton.style.top + lastRowIndex*28.3+'px';
    deleteDayButton.style.left = addDayButton.style.left;
      
    deleteDayButton.onclick = function(e) {
      socket.emit('remove_row', lastRowIndex);
      //return true;
    }
  });


  socket.on('remove_row', function(lastRowIndex){
    scheduleTable.deleteRow(lastRowIndex);
    lastRowIndex --;
    deleteDayButton = document.getElementById('deleteDayButton');
    if(lastRowIndex >= 2){
      deleteDayButton.style.position = "absolute";
      deleteDayButton.style.top = addDayButton.style.top + lastRowIndex*28.3+'px';
      deleteDayButton.style.left = addDayButton.style.left;
    } else {
      scheduleCanvas.removeChild(deleteDayButton);
    }
  });

 
  

  // draw line received from server
  
   socket.on('draw_line', function (data) {
     var line = data.line;
     
     drawingCanvasContext.beginPath();
     drawingCanvasContext.moveTo(line[0].x * width, line[0].y * height);
     drawingCanvasContext.lineTo(line[1].x * width, line[1].y * height);
     drawingCanvasContext.stroke();
  });
 */ 

messageButton.onclick = function(e) {
    console.log('button clicked' + messageInput.value);
    socket.emit('chat_message', messageInput.value);
    messageInput.value = '';
    return false;
  };

  
  socket.on('chat_message', function(msg){
    
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(msg));
    messageField.appendChild(li);
  
    
    console.log("message : " + msg);
    //$('#messages').append($('<li>').text(msg));
  });
  
  
  // main loop, running every 25ms
  
  /*
  function mainLoop() {
     // check if the user is drawing
     if (mouse.click && mouse.move && mouse.pos_prev) {
        // send line to to the server
        console.log('drawing');
        socket.emit('draw_line', { line: [ mouse.pos, mouse.pos_prev ] });
        mouse.move = false;
     }
     mouse.pos_prev = {x: mouse.pos.x, y: mouse.pos.y};
     setTimeout(mainLoop, 25);
  }
  
  mainLoop();
  */
});