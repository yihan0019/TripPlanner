var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

//var line_history = [];
var message_history = [];
var row_number = 2;
var tabel_history = [];
var map_center_lat = -25;
var map_center_lng = 131;
var location;
var label;
var marker = { 
  //lat: 0,
  //lng: 0,
  location,
  label,

}; 
var marker_list = [];

app.get('/', function(req, res){
  res.sendFile(__dirname + '/public/index.html');
});

io.on('connection', function(socket){
  console.log('a user connected');

  for (var i in message_history) {
    console.log('emit a message');
    socket.emit('chat_message', message_history[i] );
  }
 
  if(row_number >= 3){
    io.emit('add_row', row_number);
  }

  if(tabel_history.length > 1){
    for (var i in tabel_history ) {
      var indexRow = tabel_history[i].position.index_row;
      var indexCell = tabel_history[i].position.index_cell;
      var tableContent = tabel_history[i].content;
      console.log(tabel_history[i].position.index_row +' ' +tabel_history[i].position.index_cell + tabel_history[i].content);
      socket.emit('edit_table', indexRow, indexCell, tableContent);
    }
  }

  
  for (var i in marker_list){
    console.log("should reload marker" + i + " " + marker_list[i].label);
    socket.emit('add_marker', marker_list[i].location, marker_list[i].label);
  }

  io.emit('drag_map', map_center_lat, map_center_lng);
  /*
  for (var i in line_history) {
    socket.emit('draw_line', { line: line_history[i] } );
  }
  */

  socket.on('chat_message', function(msg){
    console.log('push and emit a message');
    message_history.push(msg);
    io.emit('chat_message', msg);
  });

  socket.on('add_row', function(){
    console.log('add row server side triggered');
    row_number ++;
    io.emit('add_row', row_number);
  });

  
  socket.on('remove_row', function(){
    console.log('remove row server side triggered');
    row_number --;
    io.emit('remove_row', row_number);
  });

  socket.on('edit_table', function(rowIndex, cellIndex, content){
    var tabel_content = {
      position: {index_row : rowIndex, index_cell : cellIndex},
      content: content
    };
    //tabel_content.position.index_row = rowIndex;
    //tabel_content.position.index_cell = cellIndex;
    //tabel_content.content = content;
    tabel_history.push(tabel_content);
    console.log('edit table server side triggered ' + tabel_content.position.index_row + ' ' + tabel_content.position.index_cell + tabel_content.content );
    io.emit('edit_table', rowIndex, cellIndex, content);
    console.log('edit table updated ' + tabel_history[0].position.index_row + ' ' + tabel_history[0].position.index_cell + tabel_history[0].content );

  });

  socket.on('drag_map', function(lat, lng){
    console.log('dragging map');
    map_center_lat = lat;
    map_center_lng = lng; 
    io.emit('drag_map', map_center_lat, map_center_lng);
  });



  socket.on('add_marker', function(newLocation, newLabel){
    marker = {location: newLocation, label: newLabel};
    //newMarker.lat = location.lat;
    //newMarker.lng = location.lng;
    //newMarker.location = newLocation;
    //newMarker.label = newLabel;
    console.log("marker added to list: " + marker.location + ", "  + marker.label);
    marker_list.push(marker);
    
    for (var i in marker_list){
      console.log("now in marker list" + i + " " + marker_list[i].label);
    }

    io.emit('add_marker', newLocation, newLabel);
  });

  
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
